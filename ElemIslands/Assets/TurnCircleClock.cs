﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnCircleClock : MonoBehaviour
{
    public GameObject fence;
    public GameObject fenceBoundary;
    public GameObject circle;
    public float turningSpeed = 60;
    readonly int toleranceDegrees = 15;
    //8 o clock = -140 degrees in z axis in unity, in c# its 210
    readonly int eightOclockDegrees = 240;
    readonly int closedFenceDegrees = 0;
    readonly int openFenceDegrees = -90;

    //does the player currently interact with the circle
    public bool interactionStatus { get; private set; }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void rotateCircle(float direction)
    {
        if (direction < 0)
        {
            Debug.Log("degrees z: " + circle.transform.eulerAngles.z);
            circle.transform.Rotate(Vector3.forward, turningSpeed * Time.deltaTime);
        }
        else
        {
            Debug.Log("degrees z: " + circle.transform.eulerAngles.z);
            circle.transform.Rotate(Vector3.back, turningSpeed * Time.deltaTime);
        }
    }

    public void StartInteraction()
    {
        Debug.Log("degrees z: " + circle.transform.eulerAngles.z);
        interactionStatus = true;
    }

    public void StopInteraction()
    {
        interactionStatus = false;
    }

    /*method to call after exiting the clock*/
    public bool TriggerSolutionOnExit()
    {
        bool solution = circle.transform.eulerAngles.z > eightOclockDegrees - toleranceDegrees && circle.transform.eulerAngles.z < eightOclockDegrees + toleranceDegrees;
        if (solution)
        {
            OpenFence();
			GameObject.Find ("fence_4_door").GetComponentsInChildren<Text> () [1].text = "<Press E to close>";
        }
        else
        {
            CloseFence();
			GameObject.Find ("fence_4_door").GetComponentsInChildren<Text> () [1].text = "<Press E to open>";
        }

        return solution;
    }

    public void OpenFence()
    {
        fence.transform.rotation = Quaternion.AngleAxis(openFenceDegrees, Vector3.up);
        fenceBoundary.SetActive(false);
        GameObject.Find("Open_Fence_MovieTexture_Image").GetComponent<VideoController>().startMovie();
    }

    public void CloseFence()
    {
        fence.transform.rotation = Quaternion.AngleAxis(closedFenceDegrees, Vector3.up);
        fenceBoundary.SetActive(true);
    }
}
