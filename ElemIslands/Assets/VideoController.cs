﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoController : MonoBehaviour {

    public MovieTexture movie;

    GameObject inventoryCanvas;
    GameObject hudCanvas;
    GameObject dialogueCanvas;
    GameObject imageCanvas;
    GameObject arrCanvas;

    GameObject introCanvas;
    GameObject outroCanvas;
    GameObject moleCanvas;
    GameObject fenceCanvas;

    RawImage rawImageMovie;

    GameObject mole;

    MoveBehaviour movement;

    bool videoPlaying;

	// Use this for initialization
	void Start () {
        mole = GameObject.Find("Mole");
        //movement for enabling and disabling movement
        movement = GameObject.Find("Male_idle").GetComponent<MoveBehaviour>();

        rawImageMovie = GetComponent<RawImage>();
        rawImageMovie.texture = movie;

        hudCanvas = GameObject.Find("HUDCanvas_1");
        imageCanvas = GameObject.Find("ImageCanvas");
        dialogueCanvas = GameObject.Find("DialogueCanvas");
        inventoryCanvas = GameObject.Find("InventoryCanvas");

        introCanvas = GameObject.Find("Video_Intro_Canvas");
        outroCanvas = GameObject.Find("Video_Outro_Canvas");
        moleCanvas = GameObject.Find("Video_Mole_Digging_Canvas");
        fenceCanvas = GameObject.Find("Video_Open_Fence_Canvas");
        arrCanvas = GameObject.Find("ArrowsCanvas");

        string parentName = transform.parent.name;
        if (parentName.Contains("Intro"))
        {
            startMovie();
            videoPlaying = true;
        }
        else if (parentName.Contains("Outro"))
        {
            videoPlaying = false;
            //Nothing really...
        }
        else if (parentName.Contains("Mole"))
        {
            videoPlaying = false;
        }

	}
	
	// Update is called once per frame
	void Update () {
        if(!movie.isPlaying && videoPlaying)
        {
            Debug.Log("blablablablablablabla");
            videoPlaying = false;
            stopMovie();
        }
	}

    public void startMovie()
    {
        Debug.Log("startMovie()!!");
        movement.movementEnabled = false;
        videoPlaying = true;

        //deactivate all other canvases - just in case
        //has to be changed if canvasses are added...
        inventoryCanvas.GetComponent<Canvas>().enabled = false;
        hudCanvas.GetComponent<Canvas>().enabled = false;
        dialogueCanvas.GetComponent<Canvas>().enabled = false;
        imageCanvas.GetComponent<Canvas>().enabled = false;
        arrCanvas.GetComponent<Canvas>().enabled = false;


        string parentName = transform.parent.name;
        if (parentName.Contains("Intro"))
        {
            introCanvas.GetComponent<Canvas>().enabled = true;
            outroCanvas.GetComponent<Canvas>().enabled = false;
            moleCanvas.GetComponent<Canvas>().enabled = false;
            fenceCanvas.GetComponent<Canvas>().enabled = false;
        }
        else if (parentName.Contains("Outro"))
        {
            Dbg.Trace("Game state changed: RiddleSolved");
            Dbg.Trace("Game session ended.");
            introCanvas.GetComponent<Canvas>().enabled = false;
            outroCanvas.GetComponent<Canvas>().enabled = true;
            moleCanvas.GetComponent<Canvas>().enabled = false;
            fenceCanvas.GetComponent<Canvas>().enabled = false;
        }
        else if (parentName.Contains("Mole"))
        {
            introCanvas.GetComponent<Canvas>().enabled = false;
            outroCanvas.GetComponent<Canvas>().enabled = false;
            moleCanvas.GetComponent<Canvas>().enabled = true;
            fenceCanvas.GetComponent<Canvas>().enabled = false;
        }
        else if (parentName.Contains("Fence"))
        {
            FindObjectOfType<GameStates>().fenceOpened = true;
            introCanvas.GetComponent<Canvas>().enabled = false;
            outroCanvas.GetComponent<Canvas>().enabled = false;
            moleCanvas.GetComponent<Canvas>().enabled = false;
            fenceCanvas.GetComponent<Canvas>().enabled = true;
        }

        movie.Play();

    }

    public void stopMovie()
    {
        Debug.Log("stopMovie()!!");

        if (transform.parent.name.Contains("Intro"))
        {
            Dbg.Trace("Game session started.");
        }

        movement.movementEnabled = true;

        //activate all the other canvases that are visible at the start
        //has to be changed if canvasses are added...
        inventoryCanvas.GetComponent<Canvas>().enabled = true;
        hudCanvas.GetComponent<Canvas>().enabled = true;
        dialogueCanvas.GetComponent<Canvas>().enabled = false;
        imageCanvas.GetComponent<Canvas>().enabled = false;

        introCanvas.GetComponent<Canvas>().enabled = false;
        outroCanvas.GetComponent<Canvas>().enabled = false;
        moleCanvas.GetComponent<Canvas>().enabled = false;
        fenceCanvas.GetComponent<Canvas>().enabled = false;

        movie.Stop();

        videoPlaying = false;

        if (transform.parent.name.Contains("Mole"))
        {
            FindObjectOfType<GameStates>().moleFound = true;
        }
    }

    public bool videoIsPlaying()
    {
        return movie.isPlaying;
    }
}
