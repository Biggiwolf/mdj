﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnCircleRiddle : MonoBehaviour{

    public Material halfmoonSkybox;
    public Material fullmoonSkybox;
    public Material daySkybox;

    public GameObject rainParticleSystem;
    public GameObject clouds;

    public GameObject circle;
    public float turningSpeed = 60;
    readonly int toleranceDegrees = 45;
    readonly int sunDegreesZ = 180;
    readonly int rainDegreesZ = 90;
    readonly int halfmoonDegreesZ = 0;
    readonly int fullmoonDegreesZ = 270;

    //does the player currently interact with the circle
    public bool interactionStatus { get; private set; }

	// Use this for initialization
	void Start () {


    }
	
	// Update is called once per frame
    // change the weather according to the position of the most inner circle of the riddle
	void Update () {
        if (circle.transform.rotation.eulerAngles.y > halfmoonDegreesZ - 45 && circle.transform.rotation.eulerAngles.y < halfmoonDegreesZ + 45)
        {
            RenderSettings.skybox = halfmoonSkybox;
            RenderSettings.ambientIntensity = (float)0.25;
            rainParticleSystem.SetActive(false);
            clouds.SetActive(false);
        }
        else if (circle.transform.rotation.eulerAngles.y > fullmoonDegreesZ - 45 && circle.transform.rotation.eulerAngles.y < fullmoonDegreesZ + 45)
        {
            RenderSettings.skybox = fullmoonSkybox;
            RenderSettings.ambientIntensity = (float)0.6;
            rainParticleSystem.SetActive(false);
            clouds.SetActive(false);

        }
        else if (circle.transform.rotation.eulerAngles.y > rainDegreesZ - 45 && circle.transform.rotation.eulerAngles.y < rainDegreesZ + 45)
        {
            RenderSettings.skybox = daySkybox;
            RenderSettings.ambientIntensity = (float)0.7;
            rainParticleSystem.SetActive(true);
            clouds.SetActive(true);
        }
        else
        {
            RenderSettings.skybox = daySkybox;
            RenderSettings.ambientIntensity = 1;
            rainParticleSystem.SetActive(false);
            clouds.SetActive(false);
        }
    }

    public void rotateCircle(float direction)
    {       
        if (direction < 0)
        {
            circle.transform.Rotate(Vector3.forward, turningSpeed * Time.deltaTime);
        }
        else
        {
            circle.transform.Rotate(Vector3.back, turningSpeed * Time.deltaTime);
        }
    }

    public void StartInteraction()
    {
        interactionStatus = true;
    }

    public void StopInteraction()
    {
        interactionStatus = false;
    }

    /*method to call if the wolf howls nearby the riddle, then the method can be used to check if the solution is correct*/
    public bool checkCircleOnExit()
    {
        //only check most inner circle because of soft variant
        Debug.Log("circle x: " + circle.transform.rotation.x);
        Debug.Log("circle y: " + circle.transform.rotation.y);
        Debug.Log("circle z: " + circle.transform.rotation.z);
        Debug.Log("circle euler x: " + circle.transform.rotation.eulerAngles.x);
        Debug.Log("circle euler y: " + circle.transform.rotation.eulerAngles.y);
        Debug.Log("circle euler z: " + circle.transform.rotation.eulerAngles.z);
        return circle.transform.rotation.eulerAngles.y > fullmoonDegreesZ - toleranceDegrees && circle.transform.rotation.eulerAngles.y < fullmoonDegreesZ + toleranceDegrees;
    }
}
