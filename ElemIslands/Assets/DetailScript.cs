﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/** Assign to every object that should have an image detail. It should be displayed after pressing interaction (E) 
 * on an interable object. The movement and the camera script are disabled. After pressing interaction button again, we leave the detail.
 * The changes are displayed on "ImageCanvas".
 **/

public class DetailScript : MonoBehaviour {

    public Texture2D texture;     // image to be displayed

    private Canvas imageCanvas;
    private RawImage image;
	private bool _detailShowing = false;
    private MoveBehaviour movement;
    private ThirdPersonOrbitCam mainCamera;
    private Canvas inventory;
    private Canvas dialogue;
    private Canvas hud;

    // Use this for initialization
    void Start () {
		imageCanvas = GameObject.Find("ImageCanvas").GetComponent<Canvas>();
        inventory = GameObject.Find("InventoryCanvas").GetComponent<Canvas>();
        dialogue = GameObject.Find("DialogueCanvas").GetComponent<Canvas>();
        hud = GameObject.Find("HUDCanvas_1").GetComponent<Canvas>();

        image = imageCanvas.GetComponentInChildren<RawImage>();

        imageCanvas.enabled = false;
        image.enabled = false;
        movement = GameObject.FindObjectOfType<MoveBehaviour>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>().GetComponent<ThirdPersonOrbitCam>();
    }

	public bool detailShowing{
		get { return _detailShowing; }
	}

	
	// Update is called once per frame
	void Update () {
		
	}

    public void displayObjectDetail( )
    {
		_detailShowing = true;
     //   Debug.Log("Object with detail encountered.");
        image.texture = texture;
        disableOtherCanvases();
        imageCanvas.enabled = true;            
        image.enabled = true;
        movement.movementEnabled = false;
        mainCamera.enabled = false;
    }
		
    public void hideObjectDetail( )
    {   
     //   Debug.Log("Object detail closed.");
        imageCanvas.enabled = false;
        image.enabled = false;
        enableOtherCanvases();
		_detailShowing = false;
        movement.movementEnabled = true;
        mainCamera.enabled = true;
    }


    private void disableOtherCanvases()
    {
        inventory.enabled = false;
        dialogue.enabled = false;
        hud.enabled = false;
    }

    private void enableOtherCanvases()
    {
        inventory.enabled = true;
        hud.enabled = true;
    }
}
