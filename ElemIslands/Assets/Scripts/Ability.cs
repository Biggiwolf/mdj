﻿public enum Ability
{
	Track,
    Attack,
    Dig,
    Howl,
    Off
};
