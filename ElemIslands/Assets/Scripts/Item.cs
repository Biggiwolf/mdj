﻿using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObject
{
    public Sprite sprite;
    public GameObject itemObject;

    public void Init(GameObject itemObject, Sprite sprite)
    {
        this.itemObject = itemObject;
        this.sprite = sprite;
    }

    public static Item CreateInstance(GameObject itemObject, Sprite sprite)
    {
        var data = ScriptableObject.CreateInstance<Item>();

        data.Init(itemObject, sprite);
        return data;
    }
}
