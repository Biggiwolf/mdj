﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

    public Image[] itemImages = new Image[numItemSlots];
    public Item[] items = new Item[numItemSlots];
    private GameObject inventoryCanvas;
    private bool isVisible;

    public const int numItemSlots = 4;

	// Use this for initialization
	void Start () {
        isVisible = true;
        //has to be active when first starting the game 
        inventoryCanvas = GameObject.Find("InventoryCanvas");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddItem(Item itemToAdd)
    {
        for(int i = 0; i < items.Length; i++)
        {
            if(items[i] == null)
            {
                items[i] = itemToAdd;
                itemImages[i].sprite = itemToAdd.sprite;
                itemImages[i].enabled = true;
                return;
            }
        }
    }

    public void RemoveItem(Item itemToRemove)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] == itemToRemove)
            {
                items[i] = null;
                itemImages[i].sprite = null;
                itemImages[i].enabled = false;
                return;
            }
        }
    }

    public void RemoveItem(int position)
    {
        if (position <= items.Length)
        {
            items[position] = null;
            itemImages[position].sprite = null;
            itemImages[position].enabled = false;
        }
    }

    public void HideShowInventory()
    {
        isVisible = !isVisible;
        inventoryCanvas.SetActive(isVisible);
    }

}

