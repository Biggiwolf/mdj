﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStates : MonoBehaviour {

    public Sprite flower;

    // state variables    
    private bool _moleFound = false;
    private bool _necklaceDig = false;
    private bool _necklaceTaken = false;
    private bool _moleAttacked = false;
    private bool _foxAttacked = false;
    private bool _necklaceReturned = false;
    private bool _fenceOpened = false;
    // state variables related to dialogue trigger actions
    private bool _knowsTroublesAndName = false;
    private bool _knowsTroubles = false;
    private bool _helpAccepted = false;
    private bool _knowsMoreAboutPet = false;
    private bool _knowsMoreAboutNecklace = false;
    private bool _knowsGraveHint = false;

    // references to controlled entities
    private GameObject mole;
    private GameObject fox;
    private GameObject female;
    private GameObject necklace;
    private GameObject wolf;
    private GameObject tracks;
    private Inventory inventory;


    void Start()
    {
        inventory = FindObjectOfType<Inventory>();
        mole = GameObject.Find("Mole");
        necklace = GameObject.Find("Necklace_digged");
        female = GameObject.Find("Female");
        fox = GameObject.Find("Fox");
        wolf = GameObject.Find("Wolf");
        tracks = GameObject.Find("MoleTracks");
        mole.SetActive(false);
        necklace.SetActive(false);
        fox.SetActive(false);
        tracks.SetActive(false);
        inventory.AddItem(Item.CreateInstance(null, flower));
        //helpAccepted = true;
        //moleFound = true;
    }



    // sets associated variable
    // if help is accepted in the dialogue, mole and necklace should appear in the forest
    // TO DO switch available NPC dialogue
    public bool helpAccepted
    {
        get { return _helpAccepted; }
        set
        {
			if (_helpAccepted != true && value == true) {
                Dbg.Trace("Game state changed: helpAccepted");
                // promised to help but did not ask for details
                _helpAccepted = value;
				mole.SetActive (true);
				necklace.SetActive (true);
                tracks.SetActive(true);
				NPCDialogue.setNextDialogue ("femaleDialogue3.xml");
                Dbg.Trace("Mole appeared in the game, position: " + mole.transform.position);
                Dbg.Trace("Necklace appeared in the game, position: " + necklace.transform.position);
                Dbg.Trace("Tracks appeared in the game, position: " + tracks.transform.position);
            }
        }
    }

    public bool moleFound
    {
        get { return _moleFound; }
        set
        {
            if (_moleFound != true && value == true)
            {
                Dbg.Trace("Game state changed: moleFound");
                //Debug.Log("This state was set.");
                wolf.GetComponent<MoveTo>().goAndTrackStop();
                _moleFound = value;
                mole.tag = "Attackable";
				GameObject.Find ("MoleAttackedSpot").GetComponent<UnityEngine.AI.NavMeshObstacle> ().enabled = true;
                fox.SetActive(true);
                repositionActors();
                mole.GetComponent<Animator>().SetBool("Wrap", true);
				mole.GetComponentInChildren<Canvas> ().enabled = false;
                Behaviour halo = mole.GetComponent("Halo") as Behaviour;
                halo.enabled = false;

                NPCDialogue.setNextDialogue("femaleDialogue7.xml");
                tracks.tag = "Untagged";
				GameObject.Find ("PrivatePropertyBorder").SetActive(false);
				GameObject.Find ("boundaries_garden").SetActive(false);
                Dbg.Trace("Mole repositioned, new position: " + mole.transform.position);
                Dbg.Trace("Fox appeared in the game, position: " + fox.transform.position);
            }
        }
    }

    public bool necklaceDig
    {
        get { return _necklaceDig; }
        set
        {
            if (_necklaceDig != true && value == true)
            {
                Dbg.Trace("Game state changed: necklaceDigged");
                _necklaceDig = value;
            }
        }
    }

    public bool fenceOpened
    {
        get { return _fenceOpened; }
        set
        {
            if (_fenceOpened != true && value == true)
            {
                Dbg.Trace("Game state changed: fenceOpened");
                _fenceOpened = value;
            }
        }
    }

    public bool necklaceTaken
    {
        get { return _necklaceTaken; }
        set {
                if (_necklaceTaken != true && value == true)
                {
                    _necklaceTaken = value;

                knowsMoreAboutNecklace = true;
                Dbg.Trace("Game state changed: necklaceTaken");

                if (foxAttacked && knowsGraveHint && necklaceReturned == false)
                {
                    // he did a good job with the fox and already knows about the way off the island but not about necklace
                    NPCDialogue.setNextDialogue("femaleDialogue13A.xml");
                }  else if (moleFound == false)
                {    
                    NPCDialogue.setNextDialogue("femaleDialogue12.xml");
                } else if (foxAttacked && knowsGraveHint == false)
                {                                                                

                    NPCDialogue.setNextDialogue("femaleDialogue11.xml");
                } else if (foxAttacked == false && moleAttacked == false && moleFound == true)
                {       
                    NPCDialogue.setNextDialogue("femaleDialogue7.xml");
                }               
            }
        }
    }

    public bool moleAttacked
    {
        get { return _moleAttacked; }
        set
        {
            if (_moleAttacked != true && value == true)
            {
                Dbg.Trace("Game state changed: moleAttacked");
                _moleAttacked = value;
                mole.tag = "Untagged";
                fox.tag = "Untagged";
                StartCoroutine(waitForWhenAttackedMole(2f));
                StartCoroutine(foxRunAwayWhenMoleAttacked());

                if (necklaceTaken)
                {
                    NPCDialogue.setNextDialogue("femaleDialogue9.xml");
                }
                else
                {
                    NPCDialogue.setNextDialogue("femaleDialogue8.xml");
                }
            }
        }
    }

    public bool foxAttacked
    {
        get { return _foxAttacked; }
        set
        {
            if (_foxAttacked != true && value == true)
            {
                Dbg.Trace("Game state changed: foxAttacked");
                _foxAttacked = value;
                mole.tag = "Untagged";
                fox.tag = "Untagged";
                StartCoroutine(waitForMole(6f));    
                StartCoroutine(foxRunningAway());
                if (necklaceReturned == false && necklaceTaken == true)
                {
                    NPCDialogue.setNextDialogue("femaleDialogue11.xml");
                }
                else  if (necklaceReturned && necklaceTaken)
                {
                    // he attacked fox but has no necklace
                    NPCDialogue.setNextDialogue("femaleDialogue10.xml");
				}  else if (necklaceReturned == false && necklaceTaken == false)
                {
                    NPCDialogue.setNextDialogue("femaleDialogue16.xml");
                }
            }
        }
    }

    public bool necklaceReturned
    {
        get { return _necklaceReturned; }
        set
        {
            if (_necklaceReturned != true && value == true)
            {
                Dbg.Trace("Game state changed: necklaceReturned");
                _necklaceReturned = value;
                inventory.RemoveItem(1);
                //Debug.Log("Necklace returned");

                // TODO!! another dialogue when he returns the necklace but did not find a pet - either he has alredy more infor or not
                if (moleFound == false && knowsMoreAboutPet == false)
                {
                    NPCDialogue.setNextDialogue("femaleDialogue14.xml");
                } else if (moleFound == false && knowsMoreAboutPet)
                {
                    NPCDialogue.setNextDialogue("femaleDialogue15.xml");
                }   else
                {
                    // dialogue just like that with no information
                    NPCDialogue.setNextDialogue("femaleDialogue13B.xml");
                }
                 

            }
        }
    }

	public bool knowsTroublesAndName
	{
		get { return _knowsTroublesAndName; }
		set
		{
			if (_knowsTroublesAndName != true && value == true)
			{
				_knowsTroublesAndName = value;
				GameObject.Find ("Female").GetComponentsInChildren<Text> () [0].text = "Petunia";
				NPCDialogue.setNextDialogue ("femaleDialogue2.xml"); 
			}
		}
	}

	public bool knowsTroubles
	{
		get { return _knowsTroubles; }
		set
		{
			if (_knowsTroubles != true && value == true)
			{
				_knowsTroubles = value;
				NPCDialogue.setNextDialogue ("femaleDialogue2.xml"); 
			}
		}
	}

	public bool knowsMoreAboutPet
	{
		get { return _knowsMoreAboutPet; }
		set
		{
			if (_knowsMoreAboutPet != true && value == true)
			{
				_knowsMoreAboutPet = value;
				if (_knowsMoreAboutNecklace == true) {
					NPCDialogue.setNextDialogue ("femaleDialogue6.xml");
				} else {
					NPCDialogue.setNextDialogue ("femaleDialogue4.xml");
				}
				 
			}
		}
	}

	public bool knowsMoreAboutNecklace
	{
		get { return _knowsMoreAboutNecklace; }
		set
		{
			if (_knowsMoreAboutNecklace != true && value == true)
			{
				_knowsMoreAboutNecklace = value;
				if (_knowsMoreAboutPet == true) {
					NPCDialogue.setNextDialogue ("femaleDialogue6.xml");
				} else {
					NPCDialogue.setNextDialogue ("femaleDialogue5.xml");
				}

			}
		}
	}

    public bool knowsGraveHint
    {
        get { return _knowsGraveHint; }
        set
        {
            if (_knowsGraveHint != true && value == true)
            {
                Dbg.Trace("Game state changed: knowsGraveHint");
                _knowsGraveHint = value;

				if (necklaceTaken && necklaceReturned == false) {
					NPCDialogue.setNextDialogue ("femaleDialogue13A.xml");
				} else if (necklaceTaken && necklaceReturned) {
					NPCDialogue.setNextDialogue ("femaleDialogue13B.xml");

				} else if (necklaceTaken == false && necklaceReturned == false) {
					NPCDialogue.setNextDialogue ("femaleDialogue13.xml");
				}

            }
        }
    }

    // to set states from dialogues
    // TO DO maybe remove irelevant states from there
    public void setTrueState(string state)
    {
        switch (state) {
            case "necklaceReturned":
                necklaceReturned = true;
                break;
            case "foxAttacked":
                foxAttacked = true;
                break;
            case "moleAttacked":
                moleAttacked = true;
                break;
            case "necklaceTaken":
                necklaceTaken = true;
                break;
            case "necklaceDig":
                necklaceDig = true;
                break;
            case "moleFound":
                moleFound = true;
                break;
            case "helpAccepted":
                helpAccepted = true;
                break;
			case "knowsTroublesAndName":
				knowsTroublesAndName = true;
				break;
		    case "knowsTroubles":
		    	knowsTroubles = true;
		    	break;
			case "knowsMoreAboutPet":
				knowsMoreAboutPet = true;
				break;
			case "knowsMoreAboutNecklace":
				knowsMoreAboutNecklace = true;
				break;
            case "knowsGraveHint":
                knowsGraveHint = true;
                break;
            default:
                break;
        }
    }

    // ------------------- GAME STATE TRANSITION ACTIONS ------------------------------------
    // when mole is found, fox should appear
    // female should teleport to the place where fight takes place
    // mole should appear there also
    // constants need to be adjusted!!!!!!!!!!!!!!!!!!!
    private void repositionActors()
    {
        female.gameObject.transform.position = new Vector3(16.88564f, -12.85f, -3.704909f);
        fox.gameObject.transform.Rotate(Vector3.up, 90f);
        fox.gameObject.transform.Rotate(Vector3.right, -15f);
        fox.gameObject.transform.position = new Vector3(20.7f, -13.2f, -2.7f);
        mole.gameObject.transform.position = new Vector3(21f, -14.3f, -2.7f);
        mole.gameObject.transform.Rotate(Vector3.up, -90f);
    }

    private IEnumerator waitForMole(float time)
    {
        mole.GetComponent<Animator>().SetBool("Wrap", false);
        yield return new WaitForSeconds(time);
		GameObject.Find ("MoleAttackedSpot").GetComponent<UnityEngine.AI.NavMeshObstacle> ().enabled = false;
        mole.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        mole.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 1;
        mole.GetComponent<MoveTo_otherAnimals>().enabled = true;
        
    }

    private IEnumerator foxRunningAway()
    {
        fox.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        fox.GetComponent<MoveTo_otherAnimals>().enabled = true;
        wolf.GetComponent<MoveTo>().setIndependentAction();
        yield return new WaitForSeconds(0.01f);
        wolf.GetComponent<MoveTo>().setIndependentGoal(fox.transform);
        yield return new WaitForSeconds(5f);
        wolf.GetComponent<MoveTo>().resetIndependentGoal();
        yield return new WaitForSeconds(6f);
        fox.SetActive(false);

    }

    private IEnumerator foxRunAwayWhenMoleAttacked()
    {
        fox.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        fox.GetComponent<MoveTo_otherAnimals>().enabled = true;
        yield return new WaitForSeconds(6f);
        fox.SetActive(false);
    }

    private IEnumerator waitForWhenAttackedMole(float time)
    {
        mole.GetComponent<Animator>().SetBool("Wrap", false);
        yield return new WaitForSeconds(time);
        mole.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        mole.GetComponent<MoveTo_otherAnimals>().enabled = true;
        yield return new WaitForSeconds(8);
        mole.GetComponent<Animator>().SetBool("Wrap", true);
    }


}
