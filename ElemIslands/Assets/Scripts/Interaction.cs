﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{

    private GameObject[] interactables;
    private List<GameObject> interactablesAndCollectables;
    private GameObject closestObjectBefore = null;
    private Color closestObjectBeforeMaterial;
    private Color highlightInteractable = Color.red; // the colors are not used currently because of the Halo highlight
    private Color highlightCollectable = Color.green;
    private Inventory inventory;
    private NPCDialogue d;
    private TurnCircleRiddle t;
    private TurnCircleClock c;
    private DetailScript detailedObject = null;
    private MoveBehaviour movement;
    private GameStates states;
	private bool femaleIsPissedOff = false;
    private Animator anim;
    private Canvas arr;
    private Text clockUsage;
    private Text riddleUsage;

    private bool clockRiddleSolved = false;
    private bool caveRiddleSolved = false;

    bool leftRightKeyRiddleEnabled = false;

    // Use this for initialization
    void Start()
    {
        inventory = FindObjectOfType<Inventory>();
        movement = GetComponent<MoveBehaviour>();
        states = FindObjectOfType<GameStates>();
        movement.movementEnabled = false;
        anim = GetComponent<Animator>();
        arr = GameObject.Find("ArrowsCanvas").GetComponent<Canvas>();
        arr.enabled = false;
        clockUsage = GameObject.Find("Clock").GetComponentsInChildren<Text>()[1];
        riddleUsage = GameObject.Find("Crank_Caveriddle").GetComponentsInChildren<Text>()[1];
    }

    // Update is called once per frame
    void Update()
    {
        //exit current action
        if (Input.GetButton("Cancel"))
        {
            //possibilities:
            //*npc
            if(closestObjectBefore != null)
            {
                d = closestObjectBefore.GetComponent<NPCDialogue>();
                if (d != null)
                {
                    //TODO
                }
                //*riddle
                t = closestObjectBefore.GetComponent<TurnCircleRiddle>();
                if (t != null)
                {
                    t.StopInteraction();
                    leftRightKeyRiddleEnabled = false;
                    //enable movement after not interacting with riddle anymore
                    movement.movementEnabled = true;
                }
                //*clock
                c = closestObjectBefore.GetComponent<TurnCircleClock>();
                if (c != null)
                {
                    c.StopInteraction();
                    clockRiddleSolved = c.TriggerSolutionOnExit();
                    leftRightKeyRiddleEnabled = false;
                    //enable movement after not interacting with riddle anymore
                    movement.movementEnabled = true;
                }
            }

            //stop outro if it is running
            VideoController outroMovie = GameObject.Find("Outro_MovieTexture_image").GetComponent<VideoController>();
            if(outroMovie!=null && outroMovie.videoIsPlaying())
            {
                outroMovie.stopMovie();
            }

            //stop intro if it is running
            VideoController introMovie = GameObject.Find("Intro_MovieTexture_Image").GetComponent<VideoController>();
            if (introMovie != null && introMovie.videoIsPlaying())
            {
                introMovie.stopMovie();
            }

            //stop mole video if it is running
            VideoController moleVideo = GameObject.Find("Mole_Digging_MovieTexture_Image").GetComponent<VideoController>();
            if (moleVideo != null && moleVideo.videoIsPlaying())
            {
                moleVideo.stopMovie();
            }
            //stop fence video if it is running
            VideoController fenceMovie = GameObject.Find("Open_Fence_MovieTexture_Image").GetComponent<VideoController>();
            if (fenceMovie != null && fenceMovie.videoIsPlaying())
            {
                fenceMovie.stopMovie();
            }

        }
        interactablesAndCollectables = new List<GameObject>();
        interactablesAndCollectables.AddRange(GameObject.FindGameObjectsWithTag("Interactable"));
        interactablesAndCollectables.AddRange(GameObject.FindGameObjectsWithTag("Collectable"));
		interactablesAndCollectables.AddRange(GameObject.FindGameObjectsWithTag("Digable"));

		if (!GameObject.FindObjectOfType<GameStates> ().moleFound) {
			interactablesAndCollectables.Add (GameObject.Find ("Mole"));
		}
		interactablesAndCollectables.Add(GameObject.Find("PrivatePropertyBorder"));
        GameObject closest = GetClosestEnemy(interactablesAndCollectables.ToArray());

        //every time a new closest object is appearing new
        if (closestObjectBefore == null && closest != null)
        {
            closestObjectBefore = closest;

			//Debug.Log ("Closest object: " + closest.name);
			if (closest.name == "PrivatePropertyBorder") {
				d = GameObject.Find("Female").GetComponent<NPCDialogue> ();
				if (d != null) { 
					if (!d.Talking) {
						femaleIsPissedOff = true;
						NPCDialogue.setNextDialogue ("femaleDialogue17.xml");
						d.talk ();
					}
				}

			}

			displayTextOn(closest);
			enableHighlight (closest); // highlightCollectable
            /*
            if (closest.name.Equals("Mole") && closest.tag.Equals("Trackable"))
            {
                //start digging video
             //   GameObject.Find("Mole_Digging_MovieTexture_Image").GetComponent<VideoController>().startMovie();
                StartCoroutine(moleFound());
            }
            */
          
        }
        //closest object is lost
        else if(closest == null && closestObjectBefore != null)
        {

            //end interaction with crank/riddle when going away
            t = closestObjectBefore.GetComponent<TurnCircleRiddle>();
            if (t != null)
            {
                t.StopInteraction();
                leftRightKeyRiddleEnabled = false;
                movement.movementEnabled = true;
                riddleUsage.text = "<Press E to turn>";
            }
            //end interaction with crank/riddle when going away
            c = closestObjectBefore.GetComponent<TurnCircleClock>();
            if (c != null)
            {
                c.StopInteraction();
                //clockRiddleSolved = c.TriggerSolutionOnExit();
                leftRightKeyRiddleEnabled = false;
                movement.movementEnabled = true;
                clockUsage.text = "<Press E to turn>";
            }

            //TODO exit the action of the before used object if there was an interaction
            diableHighlight(closestObjectBefore);
			displayTextOff(closestObjectBefore);
            closestObjectBefore = null;
            arr.enabled = false;
        }
        //closest object changes
        else if (closestObjectBefore != null && !closest.Equals(closestObjectBefore))
        {
            //end interaction with crank/riddle when going away
            t = closestObjectBefore.GetComponent<TurnCircleRiddle>();
            if(t != null)
            {
                t.StopInteraction();
                leftRightKeyRiddleEnabled = false;
                movement.movementEnabled = true;
                riddleUsage.text = "<Press E to turn>";

            }

            c = closestObjectBefore.GetComponent<TurnCircleClock>();
            if (c != null)
            {
                c.StopInteraction();
                //clockRiddleSolved = c.TriggerSolutionOnExit();
                leftRightKeyRiddleEnabled = false;
                movement.movementEnabled = true;
                clockUsage.text = "<Press E to turn>";

            }

            diableHighlight(closestObjectBefore);
			displayTextOff(closestObjectBefore);
            closestObjectBefore = closest;


            //Debug.Log ("Closest object: " + closest.name);
			if (closest.name == "PrivatePropertyBorder") {
				d = GameObject.Find("Female").GetComponent<NPCDialogue> ();
				if (d != null) { 
					if (!d.Talking) {
						femaleIsPissedOff = true;
						NPCDialogue.setNextDialogue ("femaleDialogue17.xml");
						d.talk ();
					}
				}

			}

			displayTextOn(closest);
            enableHighlight (closest ); // highlightCollectable

        }
        //check if a interaction should take place
        if (Input.GetButtonDown("Interact"))
        {
            if(closest != null)
            {
				if (closest.tag == "Interactable") {
					//TODO maybe make everywhere an "interact" method that is interactable and start whatever it is? 
					//TODO end dialog when pressing "esc"? 
					//TODO end interaction explicitly with e also?
					//NO NICE DISTINCTION


					if (closest.name == "fence_4_door") {
						StartCoroutine (GameObject.FindObjectOfType<Abilities> ().printTextOnScreen ("It's locked."));
					}

					detailedObject = closest.GetComponent<DetailScript> ();
					if (detailedObject != null) {

						if (detailedObject.detailShowing) {
							detailedObject.hideObjectDetail ();
						} else {
							detailedObject.displayObjectDetail ();
						}
					}


					//start dialogue with closest object and pass reference to it for the dialogue script


					d = closest.GetComponent<NPCDialogue> ();
					if (d != null) { 
						if (!d.Talking) {
							d.talk ();
						}
					}
					t = closest.GetComponent<TurnCircleRiddle> ();
					if (t != null) {
						if (t.interactionStatus == true) {
							t.StopInteraction ();

							leftRightKeyRiddleEnabled = false;
							movement.movementEnabled = true;
                            arr.enabled = false;
                            riddleUsage.text = "<Press E to turn>";
						} else { 
							t.StartInteraction ();
							leftRightKeyRiddleEnabled = t.interactionStatus;
							movement.movementEnabled = false;
                            arr.enabled = true;
                            riddleUsage.text = "<Press E to confirm>";
                        }
					}
					c = closest.GetComponent<TurnCircleClock> ();
					if (c != null) {
						if (c.interactionStatus == true) {
							c.StopInteraction ();
							clockRiddleSolved = c.TriggerSolutionOnExit ();
							leftRightKeyRiddleEnabled = false;
							movement.movementEnabled = true;
                            arr.enabled = false;
                            clockUsage.text = "<Press E to turn>";
                        } else {
							c.StartInteraction ();
							leftRightKeyRiddleEnabled = c.interactionStatus;
							movement.movementEnabled = false;
                            arr.enabled = true;
                            clockUsage.text = "<Press E to confirm>";
                        }
					}


				} else if (closest.tag == "Collectable") {
                    StartCoroutine(picking(closest));
                }
                else if (closest.tag == "Digable") {
					StartCoroutine (GameObject.FindObjectOfType<Abilities> ().printTextOnScreen ("It's too deep."));
				} else if (closest.tag == "Trackable")
                {
                    GameObject.Find("Mole_Digging_MovieTexture_Image").GetComponent<VideoController>().startMovie();
                }
                //Debug.Log("closest tag: " + closest.tag);
            }
        }
        //TODO disable player movement when rotating circles!
        if (leftRightKeyRiddleEnabled)
        {
            if (Input.GetButton("Horizontal"))
            {
                t = closest.GetComponent<TurnCircleRiddle>();

                if (t != null) { 
                    TurnCircleRiddle circle = FindObjectOfType<TurnCircleRiddle>();
                    circle.rotateCircle(Input.GetAxis("Horizontal"));
                }
                c = closest.GetComponent<TurnCircleClock>();
                if (c != null)
                {
                    TurnCircleClock circle = FindObjectOfType<TurnCircleClock>();
                    circle.rotateCircle(Input.GetAxis("Horizontal"));
                }
            }
        }

    }



    IEnumerator picking(GameObject closest)
    {
        anim.SetBool("Collect", true);
        yield return new WaitForSeconds(1f);
        inventory.AddItem(Item.CreateInstance(null, closest.GetComponent<ItemSprite>().sprite));
        if (closest.name.Equals("Necklace"))
        {
            states.necklaceTaken = true;
        }
        GameObject.Destroy(closest);
        yield return new WaitForSeconds(0.867f);
        anim.SetBool("Collect", false);
    }

    /*
     * In case of Halo, it is not possible to change the color in the script :/ 
     **/
    public void enableHighlight(GameObject obj){
		Behaviour halo = obj.GetComponent ("Halo")  as Behaviour;
		halo.enabled = true;
	}

	public void diableHighlight(GameObject obj){
		Behaviour halo = obj.GetComponent ("Halo")  as Behaviour;
		halo.enabled = false;
        if (d != null)
        {
			if (femaleIsPissedOff == true) {
				NPCDialogue.setPreviousDialogue ();
				femaleIsPissedOff = false;
			}
            d.stopTalking();
        }
	}


	public void displayTextOn(GameObject obj){
		Canvas c = obj.GetComponentInChildren<Canvas> ();
		if (c!=null && c.name.Equals ("Display")) {
			Text[] texts = c.GetComponentsInChildren<Text> ();
			foreach (Text t in texts) {
				t.enabled = true;
			} 
			c.GetComponent<TextDisplay> ().enabled = true;
		} 
	}

	public void displayTextOff(GameObject obj){
		Canvas c = obj.GetComponentInChildren<Canvas> ();
		if (c!=null && c.name.Equals ("Display")) {
			Text[] texts = c.GetComponentsInChildren<Text> ();
			foreach (Text t in texts) {
				t.enabled = false;
			} 
			c.GetComponent<TextDisplay> ().enabled = false;
		} 
	}


    GameObject GetClosestEnemy(GameObject[] enemies)
    {

        GameObject bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (GameObject potentialTarget in enemies)
        {
            
            if (potentialTarget != null && gameObjectIsVisible(potentialTarget))
            {
                
                Transform potentialTargetTransform = potentialTarget.transform;
                Vector3 directionToTarget = potentialTargetTransform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    if(closestDistanceSqr < 10)
                    {
                        bestTarget = potentialTarget;
                    }
                }
            }
        }

        return bestTarget;
    }

    //TODO also raycast if object is visible or hidden
    //other possibilities would there be to like Renderer.isVisible, but I think this is fine if not even better with racast and viewfrustum
    private bool gameObjectIsVisible(GameObject targetObject)
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(targetObject.transform.position);
        return screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
    }
}
