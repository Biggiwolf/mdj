﻿public class LostItemReaction : DelayedReaction
{

    public Item item;

    private Inventory inventory;

    protected override void ImmediateReaction()
    {
        inventory = FindObjectOfType<Inventory>();
    }

    protected override void SpecificInit()
    {
        inventory.RemoveItem(item);
    }
}

   
