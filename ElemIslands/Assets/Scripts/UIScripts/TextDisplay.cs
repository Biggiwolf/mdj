﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplay : MonoBehaviour {

	public Camera camera2;
	Text[] texts;
	// Use this for initialization
	void Start () {
		texts = GetComponentsInChildren<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		foreach (Text t in texts) {
			t.transform.rotation = Quaternion.LookRotation (t.transform.position - camera2.transform.position );
		} 
	}
}
