﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowInventory : MonoBehaviour{

    public GameObject inventoryUI;
    bool UIActive = false;

	// Use this for initialization
	void Start () {
        UIActive = false;
        inventoryUI.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //change between show inventory and dont show inventory when inventory button in the HUDCanvas is pressed
    public void inventoryButtonClicked()
    {
        //Debug.Log("Button clicked");
        if (UIActive)
        {
            inventoryUI.SetActive(false);
            UIActive = false;
        }
        else
        {
            inventoryUI.SetActive(true);
            UIActive = true;
        }
    }
}
