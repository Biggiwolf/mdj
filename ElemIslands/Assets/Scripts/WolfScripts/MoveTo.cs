﻿
// MoveTo.cs
using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class MoveTo : MonoBehaviour
{

	private Transform goal; // for storing information about goals for wolf actions
    public Transform character; // reference to main character
	private Transform sideStep;
    private float origStoppingDistance;
	private bool stepAside = false;
    UnityEngine.AI.NavMeshAgent agent;
	private Animator anim;
	private bool moveToObject {get;set; } // variable to inform update that wolf is currently moving to some action object/place
    private bool actionPerfoming { get; set; } // variable to inform update, that action is currently performed, so wolf shouldn't move anywhere
    private Abilities abilities; // reference to abilities script to trigger the action once wolf arrive to the specified place
    private bool independentGoal = false;
    private bool independentAction = false;
    private bool trackingMission = false;
    private List<Transform> steps;
    private int stepsIndex = 1;
    private bool moleFound = false;


    /**
	 * Thresholds, that trigger animation in the wolf Animator (WolfController). E.g. constant 0.6 for speed triggers walking animation.
	 * These constants are connected to those set in the Animator, so please, don't change it here, unless you change them 
	 * in the Animator accordingly.
	 * These constants do not controll the actual speed of the wolf, these are only animation "triggers"
	 * */

    private const float WALKING = 0.6f;
	private const float RUNNING = 3f;
	private const float STILL = 0f;

    // stopping distance for a goal that is not the player
    private const float SMALL_STOPPING_DISTANCE = 1.0f;

    /**
	 * If the wolf is at distance larger that this from it's goal, it starts running.
	 * */
    public  float RUNNING_DISTANCE = 3f;


    /**
     * If the wolf is closer or equal to this distance from the main character, he starts going away from him.
     * */

    public float COLIDING_DISTANCE = 1.0f;

	/**
	 * Speeds for navigation agent of the wolf. They will affect the actual moving speed of the wolf.
	 * */
	public  float WALKING_SPEED = 1f;
	public  float RUNNING_SPEED = 20.5f;


	void Start()
	{
		moveToObject = false;
        actionPerfoming = false;
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		anim = GetComponent<Animator> ();
        abilities = GetComponent<Abilities>();
        origStoppingDistance = agent.stoppingDistance;
        steps = new List<Transform>();
    }

	void Update()
	{
        //Debug.Log("ActionPerforming " + actionPerfoming + " tracking mission " + trackingMission + " step aside " + stepAside + "mole found" + moleFound);
        if (Input.GetButton("Cancel") && trackingMission == true) {
            trackingMission = false;
            anim.SetBool("Track", false);
            actionPerfoming = false;
            Dbg.Trace("Performance of ability: Track is canceled by ESC.");
            StopAllCoroutines();
        }

        if (actionPerfoming == false && independentAction == false)
        {
			if (trackingMission == false && moveToObject == false && stepAside == false)
            {
                goal = character;
				//Debug.Log("Goal: character");
            }

			if (trackingMission == false && moveToObject == false  && stepAside == true)
			{
				goal = sideStep;
				//Debug.Log("Goal: sideStep");
			}

            goTowardsGoal();
			if (!goal.Equals(character) && !goal.Equals(sideStep) && (agent.remainingDistance <= agent.stoppingDistance))
            {
                actionPerfoming = true;
                if (trackingMission == false)
                {
                    abilities.performAction();
                }  else
                {
                    waitTrack();
                }
            }
        }  else if (independentGoal == true)
        {
            //Debug.Log("Going towards independent goal.");
            goTowardsIndependentGoal();
        }
    }

    private IEnumerator ComputeDistance(float seconds)
    {
        for (float i = 0; i < seconds; i += 0.5f)
        {
            double y = character.transform.position.y - transform.position.y;
            double x = character.transform.position.x - transform.position.x;
            double z = character.transform.position.z - transform.position.z;
            double distance = System.Math.Sqrt(y * y + x * x + z * z);
            Dbg.Trace("Player vs. Wolf distance: " + distance + ", wolf visible on camera " + GetComponent<Renderer>().isVisible);
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void waitTrack()
    {
        if (moleFound)
        {
            goAndTrackStop();
        }
        else if (stepsIndex < 4)
        {
            anim.SetBool("Track", true);
            StartCoroutine(ComputeDistance(5f));
            Invoke("increaseStepIndex", 5f);
        }
        else
        {
            anim.SetFloat("Speed", STILL);
        }
    }

    public void setIndependentAction()
    {
       independentAction = true;
    }

    public void setIndependentGoal(Transform iGoal)
    {
        agent.destination = iGoal.position;
        agent.speed = 0;
        StopAllCoroutines();
        agent.stoppingDistance = origStoppingDistance;
        stepAside = false;
        goal = iGoal;
        anim.SetFloat("Speed", STILL);
        Invoke("activateIndependentGoal", 0.7f);
    }

    private void activateIndependentGoal()
    {
        independentGoal = true;
    }


    public void resetIndependentGoal()
    {
        actionPerfoming = false;
        independentGoal = false;
        independentAction = false;
        goal = character;
    }

    private void goTowardsIndependentGoal()
    {
        agent.destination = goal.position;
        agent.gameObject.transform.LookAt(goal);
        agent.gameObject.transform.Rotate(new Vector3(0, 180, 0), Space.Self);
        //Debug.Log("DISTANCE " + agent.remainingDistance + " " + agent.stoppingDistance);
        if (agent.remainingDistance > agent.stoppingDistance)
        {
            agent.speed = RUNNING_SPEED;
            anim.SetFloat("Speed", RUNNING);
        }  
    }	

	public IEnumerator GoAway(){
		float walkRadius = 5;

		Vector3 randomDirection = Random.onUnitSphere * walkRadius;
		randomDirection += agent.gameObject.transform.position;
        NavMeshHit hit;
		NavMesh.SamplePosition  (randomDirection, out hit, walkRadius, 1);
        //Debug.Log("Hit position:" + hit.position);
        if (!hit.position.Equals(new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity)))
        {
            GameObject go = new GameObject();
            go.gameObject.transform.position = hit.position;
            sideStep = go.gameObject.transform;
            stepAside = true;
            goal = sideStep;
            //Debug.Log("step aside = true");
            
            agent.stoppingDistance = SMALL_STOPPING_DISTANCE;
            while (agent.remainingDistance >= agent.stoppingDistance)
            {
                yield return new WaitForSeconds(0.1f);
            }
            agent.stoppingDistance = origStoppingDistance;
            goal = character;
            stepAside = false;
            //Debug.Log("step aside = false");
        }
	}

	public void goTowardsGoal(){

        agent.destination = goal.position;
		//Debug.Log ("going towards:" + goal.position);
		//Debug.Log("remaining distance: " + agent.remainingDistance);

        agent.gameObject.transform.LookAt(goal);
        agent.gameObject.transform.Rotate(new Vector3(0, 180, 0), Space.Self);

        if (agent.remainingDistance <= agent.stoppingDistance) {
			anim.SetFloat("Speed", STILL);

      /*      if (goal.Equals(character) && (agent.remainingDistance <= COLIDING_DISTANCE)) {
				//Debug.Log("wolf in the way, remaining distance: " + agent.remainingDistance);
			//	Debug.Log ("Condition is: " + (agent.remainingDistance <= COLIDING_DISTANCE));
		//		Debug.Log ("Coliding distance is: " + COLIDING_DISTANCE);
				if (stepAside == false) {
					StartCoroutine (GoAway ());
				}
			} */
		} else {
			if (agent.remainingDistance <= RUNNING_DISTANCE) {
				agent.speed = WALKING_SPEED;
				anim.SetFloat ("Speed", WALKING); // we set "Speed" parameter int the animator to the specified constant, so that it triggers walking animation
			} else {
				agent.speed = RUNNING_SPEED;
				anim.SetFloat("Speed", RUNNING);
			}
		}



	}

	public void startAbilityUsed()
	{
		//Debug.Log("startAbilityUsed");
		moveToObject = true;
        agent.stoppingDistance = SMALL_STOPPING_DISTANCE;
	}

	public void endAbilityUsed()
	{
		//Debug.Log("endAbilityUsed");
		moveToObject = false;
        actionPerfoming = false;
        agent.stoppingDistance = origStoppingDistance;
	}

    //move the wolf towards the selected object or point where it should perform an action
    public void moveToAction(Transform place)
    {
        if (trackingMission == true)
        {
            StopAllCoroutines();
            Dbg.Trace("Performing ability Track was interupted.");
        }
        trackingMission = false;
        anim.SetBool("Track", false);
        actionPerfoming = false;
        startAbilityUsed();
        goal = place; // set a new goal
        agent.destination = goal.position; //change wolf desiring destination to goal position 
    }

    public void goAndTrackStop()
    {
        StopAllCoroutines();
        trackingMission = false;
        anim.SetBool("Track", false);
        actionPerfoming = false;
        moleFound = true;        
    }

    private void increaseStepIndex()
    {
        if (trackingMission == true)
        {
            actionPerfoming = false;
            anim.SetBool("Track", false);
            goal = GameObject.Find("WolfTrackStop" + stepsIndex).transform;
            agent.destination = goal.position;
            stepsIndex++;
        }
    }

    public void goAndTrack()
    {
        stepsIndex = 1;
        actionPerfoming = false;
        anim.SetBool("Track", false);
        trackingMission = true;
        //Debug.Log("Step index: " + stepsIndex);
        goal = GameObject.Find("WolfTrackStop" + stepsIndex).transform;
        agent.destination = goal.position;
        stepsIndex++;
    }

}

