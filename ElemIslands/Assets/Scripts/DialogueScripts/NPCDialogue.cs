﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Serialization;
using System.IO;


public class NPCDialogue : MonoBehaviour {

	Dialogue dialogue;
	Text dialogueText;
	GameObject dialoguePanel;
	Canvas dialogueCanvas;
	Canvas inventoryCanvas;
	Canvas HUDCanvas;

    private MoveBehaviour movement;
    private ThirdPersonOrbitCam mainCamera;
    private GameStates states;

    Button[] answers;

	bool talking = false;
	int selectedOption;

	private static string nextDialogue = "femaleDialogue1.xml";
	private static string previousDialogue = "";

	// Use this for initialization
	void Start () {
		selectedOption = 0;
		GameObject d = GameObject.Find ("DialogueCanvas");
		dialogueCanvas = d.GetComponent<Canvas> ();
		dialogueCanvas.enabled = false;


		dialoguePanel = GameObject.Find ("DialoguePanel");
		dialogueText = GameObject.Find ("DialogueText").GetComponent<Text>();

		answers = dialoguePanel.GetComponentsInChildren<Button> ();

		foreach (Button answer in answers){
			answer.gameObject.SetActive (false);
		}

		talking = false;

		//while conversation, diable all other canvases and let only the conversation canvas be visible
		HUDCanvas = GameObject.Find ("HUDCanvas_1").GetComponent<Canvas> ();
		inventoryCanvas = GameObject.Find ("InventoryCanvas").GetComponent<Canvas> ();

        // find objects to manipulate with player movement
        movement = GameObject.FindObjectOfType<MoveBehaviour>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>().GetComponent<ThirdPersonOrbitCam>();

        states = FindObjectOfType<GameStates>();
    }


    private void disablePlayerMovement()
    {
        movement.movementEnabled = false;
        mainCamera.enabled = false;
    }

    private void enablePlayerMovement()
    {
        movement.movementEnabled = true;
        mainCamera.enabled = true;
    }


    /**
	 * This method runs in the loop since the object, which has this dialogue script, exists.
	 * The "talkingStart" is set to true from "Interaction" script, so the object starts talking only after interaction with
	 * the player is made.
	 **/
    void Update () {
	}

    public void talk()
    {
        StartCoroutine(run());
    }

    public void stopTalking()
    {
        
        StopAllCoroutines();
        restoreCanvas();
        enablePlayerMovement();
    }

	public bool Talking {
		get {
			return this.talking;
		}
		set {
			talking = value;
		}
	}

	public static void setNextDialogue(string next){
		previousDialogue = nextDialogue;
		nextDialogue = next;
	}

	public static void setPreviousDialogue(){
		nextDialogue = previousDialogue;
	}

	public IEnumerator run(){
        disablePlayerMovement();
        Quaternion rotation = transform.rotation;
        transform.LookAt(GameObject.Find("Male_idle").transform);
        talking = true;
		dialogue = loadDialogue ("Assets/Dialogues/" + nextDialogue);
		// switch off all other canvases and switch on the dialog canvas
		dialogueCanvas.enabled = true;
		inventoryCanvas.enabled = false;
		HUDCanvas.enabled = false;
		// TODO - disable moving of the player
		// TODO - swith camera to different angle
	//	GameObject.Find("Ethan").GetComponent<MoveBehaviour> ().enabled = false;
		//GameObject.Find("Ethan").GetComponent<BasicBehaviour> ().enabled = false;
	//	switchDialogCamera (true);

		// traverse the XML of the conversation

		foreach (Button answer in answers){
			answer.gameObject.SetActive (false);
		}

		int nodeId = 0;

		//debugPrintAllNodes ();

		while (nodeId != -1) {
            states.setTrueState(dialogue.nodes[nodeId].triggerAction);

            // display XML structure to the canvas
            displayNode (dialogue.nodes [nodeId]);
			SetSelectedOption (-2);

			//Debug.Log ("What is my id before:" + nodeId);

			// wait for the player to choose an option
			while (selectedOption == -2) {
				yield return new WaitForSeconds (0.15f);
			}
			nodeId = selectedOption;
			//Debug.Log ("What is my id after:" + nodeId);

		}
        transform.rotation = rotation;
        restoreCanvas();
        enablePlayerMovement();



        //	GameObject.Find("Ethan").GetComponent<MoveBehaviour> ().enabled = true;
        //GameObject.Find("Ethan").GetComponent<BasicBehaviour> ().enabled = true;
        //switchDialogCamera (false);
    }

    private void restoreCanvas()
    {
        dialogueCanvas.enabled = false;
        inventoryCanvas.enabled = true;
        HUDCanvas.enabled = true;
        SetSelectedOption(-2);
        talking = false;
    }

    public void SetSelectedOption(int o){		
		selectedOption = o;
	//	Debug.Log("Selected option = " + o);
	}

	// sets all the answers and text to the canvas and makes buttons visible 
	// (maximum number of the answers is limited by the number of buttons created in the canvas)
	private void displayNode(DialogueNode node){
		dialogueText.text = node.text;

		foreach (Button answer in answers){
			answer.gameObject.SetActive (false);
		}

		for (int i = 0; i < node.options.Count && i < answers.Length; i++) {
			//Debug.Log ("Processing index:" + i);
			answers [i].gameObject.SetActive (true);
			//answers [i].GetComponentInChildren<Text> ().enabled = true;
			answers [i].GetComponentInChildren<Text> ().text = node.options [i].answer;
			int following = node.options [i].destinationNodeID;
			// action to be performed when player clicks a button with answer
			answers [i].onClick.AddListener (delegate {
			//	Debug.Log("Clicked");
				SetSelectedOption(following);
			}); 
		}


	}

	private void debugPrintAllNodes() {
		for (int i = 0; i < dialogue.nodes.Count; i++) {
			Debug.Log (i + " " + dialogue.nodes [i].text);
		}
	}


	// load dialogue from the XML and convert it
	public static Dialogue loadDialogue(string path){
		XmlSerializer ser = new XmlSerializer (typeof(Dialogue));
		StreamReader reader = new StreamReader (path);

		Dialogue d = (Dialogue)ser.Deserialize (reader);

	//	Debug.Log (d.ToString ());

		return d;
	}



}
