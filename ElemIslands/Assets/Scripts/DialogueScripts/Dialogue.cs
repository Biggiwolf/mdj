﻿
using System.Collections.Generic;

public class Dialogue {

	public List<DialogueNode> nodes;

	public override string ToString ()
	{
		return string.Format ("[Dialogue: nodes={0}]", nodes);
	}
	
 
}
