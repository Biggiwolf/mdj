﻿
using System.Collections.Generic;

public class DialogueNode  {
    public string triggerAction;
	public int nodeID = -1;
	public string text;
	public List<DialogueOption> options;

	public override string ToString ()
	{
		return string.Format ("[DialogueNode: nodeID={0}, text={1}, options={2}]", nodeID, text, options);
	}
	
}
