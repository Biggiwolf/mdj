﻿
using System.Collections.Generic;

public class DialogueOption {
	public string answer;
	public int destinationNodeID;

	public override string ToString ()
	{
		return string.Format ("[DialogueOption: answer={0}, destinationNodeID={1}]", answer, destinationNodeID);
	}
	
}
