﻿using UnityEngine;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;

public class Dbg : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------------------------
	public		string			LogFile = "log";
	public		bool			EchoToConsole = true;
	public		bool			AddTimeStamp = true;
	public      string          LogDirectory = "log";

	//-------------------------------------------------------------------------------------------------------------------------
	private		StreamWriter	OutputStream;

	//-------------------------------------------------------------------------------------------------------------------------
	static Dbg Singleton = null;

	//-------------------------------------------------------------------------------------------------------------------------
	public static Dbg Instance
	{
		get { return Singleton; }
	}

	//-------------------------------------------------------------------------------------------------------------------------
	void Awake()
	{

		if (Singleton != null)
		{
			UnityEngine.Debug.LogError("Multiple Dbg Singletons exist!");
			return;
		}

		Singleton = this;

		// Open the log file to append the new log to it.
		System.IO.Directory.CreateDirectory(LogDirectory);
		LogFile = LogDirectory + "\\" + LogFile;
		LogFile += DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
		LogFile += ".txt";
		OutputStream = new StreamWriter( LogFile, true );
	}

	//-------------------------------------------------------------------------------------------------------------------------
	void OnDestroy()
	{
		if ( OutputStream != null )
		{
			OutputStream.Close();
			OutputStream = null;
		}
	}

	//-------------------------------------------------------------------------------------------------------------------------
	private void Write( string message )
	{
		if ( AddTimeStamp )
		{
			DateTime now = DateTime.Now;
			message = string.Format("[{0:H:mm:ss:fff}] {1}", now, message );
		}

		if ( OutputStream != null )
		{

			OutputStream.WriteLine( message );
			OutputStream.Flush();
		}

		if ( EchoToConsole )
		{
			UnityEngine.Debug.Log( message );
		}
	}

	//-------------------------------------------------------------------------------------------------------------------------
	//[Conditional("DEBUG"), Conditional("PROFILE")]
	public static void Trace( string Message)
	{
		if ( Dbg.Instance != null )
			Dbg.Instance.Write( Message );
		else
			// Fallback if the debugging system hasn't been initialized yet.
			UnityEngine.Debug.Log( Message );
	}


}
