﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Abilities : MonoBehaviour
{
    public Texture2D attackCursor;
    public Texture2D actionsCursor;

    Ability activeAbility; // which ability wolf tries to perform
    Ability clickedAbility; // which ability is clicked on gui and waiting to select a point

    private MoveTo moveTo;
    private Animator anim;

    private GameObject attackTarget;
    public GameObject necklace;
    private GameObject player;

    private Text text;
    private float trackingRadius = 5;
    private float howlRadius = 40;
    private float digRadius = 20;

    private GameStates states;

    private bool fromAnimate = false;


    // Use this for initialization
    void Start()
    {
        activeAbility = Ability.Off;
        clickedAbility = Ability.Off;
        moveTo = GetComponent<MoveTo>();
        anim = GetComponent<Animator>();
        text = GameObject.Find("informative-text").GetComponent<Text>();
        states = FindObjectOfType<GameStates>();
        player = GameObject.Find("Male_idle");
    }


    // Update is called once per frame
    void Update()
    {
        //exit current action
        if (Input.GetButton("Cancel") && activeAbility != Ability.Off)
        {
            Dbg.Trace("Performance of ability: " + activeAbility + " is canceled by ESC.");
            //Debug.Log("escape current action");
            stopCurrentAction();
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            clickedAbility = Ability.Off;
        }

        // some activity selected on gui
        if (clickedAbility != Ability.Off)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log("mouse click");
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {

                    if (clickedAbility == Ability.Attack)
                    {
                        Dbg.Trace("Target for ability: " + clickedAbility + " is selected.");
                        // can attack only interactable objects
                        if (hit.collider.gameObject.tag.Equals("Attackable"))
                        {
                            // stop other action if it is performing
                            stopCurrentAction();
                            activeAbility = clickedAbility;
                            //hit.collider.gameObject.transform - vector position where the wolf will attack
                            moveTo.moveToAction(hit.collider.gameObject.transform);
                            Dbg.Trace("Target coordinates: " + hit.collider.gameObject.transform.position);
                            Dbg.Trace("Selected attacking position is valid.");
                            attackTarget = hit.collider.gameObject;
                        }
                        else
                        {
                            Dbg.Trace("Target coordinates: " + hit.point);
                            Dbg.Trace("Selected attacking position is invalid.");
                            //Debug.Log("There is nothing to attack!");
                            StartCoroutine(printTextOnScreen("There is nothing to attack..."));
                        }
                    }
                    else if (clickedAbility == Ability.Dig || clickedAbility == Ability.Track || clickedAbility == Ability.Howl)
                    {
                        // stop other action if it is performing
                        stopCurrentAction();
                        Dbg.Trace("Target for ability: " + clickedAbility + " is selected.");
                        activeAbility = clickedAbility;
                        Transform goal = new GameObject("Target").transform;
                        //goal.position - position where the wolf will use ability
                        goal.position = hit.point;
                        Dbg.Trace("Target coordinates: " + hit.point);
                        moveTo.moveToAction(goal);
                    }
                    else
                    {
                        Debug.LogError("current ability wasnt off but neither attack, dig, track or howl");
                    }
                }
                Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
                clickedAbility = Ability.Off;
            }
        }
    }

    public IEnumerator printTextOnScreen(string textToSet)
    {
        text.text = textToSet;
        yield return new WaitForSeconds(2);
        text.text = "";
    }

    // use for animation of certain action, in future can be renamed and used for actual actions
    public IEnumerator Animate(string actionName, float seconds)
    {
        anim.SetBool(actionName, true);
       // Debug.Log(actionName + " action called");
        yield return new WaitForSeconds(seconds);
        //Debug.Log(actionName + " finished.");
        fromAnimate = true;
        stopCurrentAction();
    }

    private IEnumerator ComputeDistance(float seconds)
    {
        for (float i = 0; i<seconds; i+=0.5f)
        {
            double y = player.transform.position.y - transform.position.y;
            double x = player.transform.position.x - transform.position.x;
            double z = player.transform.position.z - transform.position.z;
            double distance = Math.Sqrt(y * y + x * x + z * z);
            Dbg.Trace("Player vs. Wolf distance: " + distance + ", wolf visible on camera " + GetComponent<Renderer>().isVisible);
            yield return new WaitForSeconds(0.5f);
        }
    }

    // select correct action to perform
    public void performAction()
    {
        //Debug.Log("Wolf performing action" + activeAbility);
        if (activeAbility == Ability.Dig)
        {
            StartCoroutine(Animate(activeAbility.ToString(), 2.367f));
            StartCoroutine(ComputeDistance(2.367f));
            Invoke("performDelayedDig", 2.367f);
        }
        else if (activeAbility == Ability.Track)
        {
            StartCoroutine(Animate(activeAbility.ToString(), 5.8f));
            StartCoroutine(ComputeDistance(5.8f));
            Invoke("performDelayedTrack", 5.8f);
        }
        else if (activeAbility == Ability.Howl)
        {
            StartCoroutine(ComputeDistance(3.25f));
            StartCoroutine(Animate(activeAbility.ToString(), 3.25f));
            Invoke("performDelayedHowl", 3.25f);
        }
        else if (activeAbility == Ability.Attack)
        {
            StartCoroutine(ComputeDistance(1.7f));
            StartCoroutine(Animate(activeAbility.ToString(), 1.7f));
            //Debug.Log("attack game object: " + attackTarget);
            // added there because it is not turning to the wolf directly
            if (attackTarget.name.Equals("Fox"))
            {
                attackTarget.transform.LookAt(transform);
                attackTarget.transform.Rotate(new Vector3(0, 180, 0), Space.Self);
            }
            Invoke("performDelayedAttack", 1.7f);
        }
    }

    // stops running action
    private void stopCurrentAction()
    {
        if (activeAbility != Ability.Off)
        {   if (!fromAnimate)
            {
                Dbg.Trace("Performing of ability " + activeAbility + " is interupted.");
                fromAnimate = false;
            } else
            {
                fromAnimate = false;
                //Dbg.Trace("Performing of ability " + activeAbility + " is completed.");
            }
            anim.SetBool(activeAbility.ToString(), false);
            //Debug.Log("Stoping " + activeAbility.ToString());
            activeAbility = Ability.Off;
            StopAllCoroutines();
            moveTo.endAbilityUsed();
        }
    }

    GameObject CheckIfObjectIsInRadius(GameObject[] trackables, float searchRadius)
    {
        GameObject bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (GameObject potentialTarget in trackables)
        {
            Transform potentialTargetTransform = potentialTarget.transform;
            Vector3 directionToTarget = potentialTargetTransform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                if (closestDistanceSqr < searchRadius)
                {
                    bestTarget = potentialTarget;
                }
            }

        }

        return bestTarget;
    }

    /*---------------- Delayed Actions for Abilities after the animation -------------------------- */
    public void performDelayedDig()
    {
        GameObject[] digables = GameObject.FindGameObjectsWithTag("Digable");
        GameObject trackedObject = CheckIfObjectIsInRadius(digables, trackingRadius);
        if (activeAbility == Ability.Dig)
        {
            if (trackedObject != null)
            {
                //Debug.Log("FOUND SOMETHING DIGABLE");
                Dbg.Trace("Selected digging position is valid");
                GameObject.Destroy(trackedObject);
                necklace.transform.position = new Vector3(34.071f, -15.043f, 10.932f);
                necklace.SetActive(true);
                states.necklaceDig = true;
            }
            else
            {
                Dbg.Trace("Selected digging position is invalid");
            }
        }
    }

    private void performDelayedAttack()
    {
        // Moved to perform action, because it needs to turn to the wolf in the beginnning
        //attackTarget.transform.LookAt(transform);
        //attackTarget.transform.Rotate(new Vector3(0, 180, 0), Space.Self);    // because fox has "z" axis in the direction of tail
        //TODO 2.) thing starts attack routine also
        if (attackTarget.name.Equals("Mole"))
        {
            states.moleAttacked = true;
        }
        else
        {
            //3.) remove health of wolf
            GetComponent<PlayerHealth>().hitObject(15);
            //TODO 4.) thing runs away
            states.foxAttacked = true;
        }
        attackTarget = null;
    }

    private void performDelayedHowl()
    {
        GameObject caveRiddle = GameObject.Find("Caveriddle");
        Vector3 currentPosition = transform.position;
        Vector3 directionToTarget = caveRiddle.transform.position - currentPosition;
        float dSqrToTarget = directionToTarget.sqrMagnitude;

        if (activeAbility == Ability.Howl)
        {
            if ((dSqrToTarget < howlRadius) && FindObjectOfType<TurnCircleRiddle>().checkCircleOnExit())
            {
                Dbg.Trace("Selected howling position is valid.");
                //start outro
                GameObject.Find("Outro_MovieTexture_image").GetComponent<VideoController>().startMovie();
            }
            else
            {
                Dbg.Trace("Selected howling position is invalid");
            }
        }
    }

    public void performDelayedTrack()
    {
        GameObject[] trackables = GameObject.FindGameObjectsWithTag("Trackable");
        GameObject trackedObject = CheckIfObjectIsInRadius(trackables, trackingRadius);
        if (activeAbility == Ability.Track)
        {

            if (trackedObject != null && trackedObject.name.Equals("MoleTracks"))
            {
                Dbg.Trace("Selected tracking position is valid.");
                moveTo.goAndTrack();
            }
            else
            {
                Dbg.Trace("Selected tracking position is invalid.");
            }
        }
    }


    /*---------------- UI Controller Section -------------------------- */
    public void attack()
    {
        //Dbg.Trace("Ability clicked:" + clickedAbility);
        //Debug.Log("attack button pressed on ui");
        clickedAbility = Ability.Attack;
        Dbg.Trace("Ability clicked:" + clickedAbility);
        Cursor.SetCursor(attackCursor, Vector2.zero, CursorMode.Auto);
    }

    public void track()
    {
        //Debug.Log("track button pressed on ui");
        clickedAbility = Ability.Track;
        Dbg.Trace("Ability clicked:" + clickedAbility);
        Cursor.SetCursor(actionsCursor, Vector2.zero, CursorMode.Auto);
    }

    public void howl()
    {
        //Debug.Log("howl button pressed on ui");
        clickedAbility = Ability.Howl;
        Dbg.Trace("Ability clicked:" + clickedAbility);
        Cursor.SetCursor(actionsCursor, Vector2.zero, CursorMode.Auto);
    }

    public void dig()
    {
        //Debug.Log("dig button pressed on ui");
        clickedAbility = Ability.Dig;
        Dbg.Trace("Ability clicked:" + clickedAbility);
        Cursor.SetCursor(actionsCursor, Vector2.zero, CursorMode.Auto);
    }

}
