﻿// MoveTo.cs
using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class MoveTo_otherAnimals : MonoBehaviour
{

    public bool roation = false;
    private Transform goal; // for storing information about goals for wolf actions
    public Transform character; // reference to main character
    UnityEngine.AI.NavMeshAgent agent;
    private Animator anim;
    private bool moveToObject { get; set; } // variable to inform update that wolf is currently moving to some action object/place
    bool stopped = false;
    private bool firstTime = true;

    //   private bool foxAttacked = true;


    void Start()
    {
        moveToObject = false;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        goal = character;
    }

    void Update()
    {
        if (!stopped)
        {
            goTowardsGoal();
        }
    }

    public void goTowardsGoal()
    {

        agent.gameObject.transform.LookAt(goal);
        if (roation)
        {
            agent.gameObject.transform.Rotate(new Vector3(0, 180, 0), Space.Self);
        }

        agent.destination = goal.position;

        anim.SetBool("Walk", true);

        if (agent.remainingDistance != 0 && agent.remainingDistance <= agent.stoppingDistance)
        {
            Debug.Log(agent.remainingDistance);
            Debug.Log(agent.stoppingDistance);
            Debug.Log("Stopping walking");
            anim.SetBool("Walk", false);
            stopped = true;
            agent.gameObject.transform.Rotate(new Vector3(0, -90, 0), Space.Self);
        }
    }


}