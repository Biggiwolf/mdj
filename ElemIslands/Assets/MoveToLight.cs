﻿
// MoveTo.cs
using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class MoveToLight : MonoBehaviour
{

    private Transform goal; // for storing information about goals for wolf actions
    //public Transform character; // reference to main character
    private Transform sideStep;
    private float origStoppingDistance;
    private bool stepAside = false;
    UnityEngine.AI.NavMeshAgent agent;
    private Animator anim;
    private bool moveToObject { get; set; } // variable to inform update that wolf is currently moving to some action object/place
    private bool actionPerfoming { get; set; } // variable to inform update, that action is currently performed, so wolf shouldn't move anywhere
    private Abilities abilities; // reference to abilities script to trigger the action once wolf arrive to the specified place

    /**
	 * Thresholds, that trigger animation in the wolf Animator (WolfController). E.g. constant 0.6 for speed triggers walking animation.
	 * These constants are connected to those set in the Animator, so please, don't change it here, unless you change them 
	 * in the Animator accordingly.
	 * These constants do not controll the actual speed of the wolf, these are only animation "triggers"
	 * */

    private const float WALKING = 0.6f;
    private const float RUNNING = 3f;
    private const float STILL = 0f;

    // stopping distance for a goal that is not the player
    private const float SMALL_STOPPING_DISTANCE = 0.5f;

    /**
	 * If the wolf is at distance larger that this from it's goal, it starts running.
	 * */
    public float RUNNING_DISTANCE = 3f;


    /**
     * If the wolf is closer or equal to this distance from the main character, he starts going away from him.
     * */

    public float COLIDING_DISTANCE = 1.0f;

    /**
	 * Speeds for navigation agent of the wolf. They will affect the actual moving speed of the wolf.
	 * */
    public float WALKING_SPEED = 0.5f;
    public float RUNNING_SPEED = 0.5f;


    void Start()
    {
        moveToObject = false;
        actionPerfoming = false;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();
        abilities = GetComponent<Abilities>();
        origStoppingDistance = agent.stoppingDistance;
        Invoke("foxMoveToActionAfterAttack", 8);
    }

    void Update()
    {
        
    }
    



    //move the wolf towards the selected object or point where it should perform an action
    public void moveToAction(Transform place)
    {
        Debug.Log("moveToAction in MoveTo script called");
        goal = place; // set a new goal
        agent.destination = goal.position; //change wolf desiring destination to goal position 

    }

    //no parameters if function should be called via Invoke
    public void foxMoveToActionAfterAttack()
    {
        Transform aimFox = GameObject.Find("Aim_Fox_To_Run").transform;
        moveToAction(aimFox);
    }

}

