﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnTrigger : MonoBehaviour {

	public Text text;
	public int value;
	public TextMesh text3d;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	} 


	void OnTriggerEnter (Collider other) {
		if (other.gameObject.name.Contains("Ethan")){
			text.text = "Collision";
			text3d.text = "Sphere";
			Debug.Log("Enter called.");
		}
	}

	void OnTriggerStay (Collider other) {
		if (other.gameObject.name.Contains("Ethan")){
			text.text = "Collision";
			text3d.text = "Sphere";
			Debug.Log("Stay called.");
		}
	}

	void OnTriggerExit (Collider other) {
		if (other.gameObject.name.Contains("Ethan")){
			text.text = "";
			text3d.text = "";
			Debug.Log("Exit called.");
		}
	}

}
