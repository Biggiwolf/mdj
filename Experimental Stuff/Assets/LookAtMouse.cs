﻿//source: http://answers.unity3d.com/questions/855976/make-a-player-model-rotate-towards-mouse-location.html

using UnityEngine;
using System.Collections;

public class LookAtMouse : MonoBehaviour
{

    // speed is the rate at which the object will rotate
    public float speed;

    void FixedUpdate()
    {
        if (Camera.main == null)
            print("camera null");
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        float angleX = AngleBetweenTwoPointsX(positionOnScreen, mouseOnScreen);
        float angleY = AngleBetweenTwoPointsY(positionOnScreen, mouseOnScreen);

        //transform.rotation = Quaternion.Euler(new Vector3(angleY * speed, -angleX * speed, 0f));
        transform.rotation = Quaternion.Euler(new Vector3(0f, -angleX * speed, 0f));

        /*
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.current.ScreenPointToRay(Input.mousePosition);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Smoothly rotate towards the target point.
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
        }
        */
        
    }

    float AngleBetweenTwoPointsX(Vector3 a, Vector3 b)
    {
        //return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
        return Mathf.Atan(a.x - b.x) * Mathf.Rad2Deg;
    }

    float AngleBetweenTwoPointsY(Vector3 a, Vector3 b)
    {
        //return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
        return Mathf.Atan(a.y - b.y) * Mathf.Rad2Deg;
    }
}
