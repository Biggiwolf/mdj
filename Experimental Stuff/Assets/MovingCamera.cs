﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCamera : MonoBehaviour {

    float yaw = 0.0f;
    float pitch = 0.0f;
    float xAxisMove;
    float yAxisMove;
    public float movingSpeed;
    public float mouseSensitivity;

	// Use this for initialization
	void Start () {
        movingSpeed = 10f;
        mouseSensitivity = 4f;
	}
	
	// Update is called once per frame
	void Update () {
        xAxisMove = Input.GetAxis("Horizontal");
        yAxisMove = Input.GetAxis("Vertical");
        transform.Translate(movingSpeed * xAxisMove * Time.deltaTime, 0f, movingSpeed * yAxisMove * Time.deltaTime);
        
        yaw += mouseSensitivity * Input.GetAxis("Mouse X");
        pitch -= mouseSensitivity * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        
    }

}
