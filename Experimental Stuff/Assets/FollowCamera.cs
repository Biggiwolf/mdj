﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    public GameObject target;
    Vector3 offset;

	// Use this for initialization
	void Start () {
        offset = target.transform.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LateUpdate()
    {
        float desiredAngle = target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
        transform.position = target.transform.position - (rotation * offset);
        Vector3 targetPostition = new Vector3(target.transform.position.x,
                                       this.transform.position.y,
                                       target.transform.position.z);
        transform.LookAt(targetPostition);
        //transform.LookAt(target.transform);
    }

}
