﻿
// MoveTo.cs
using UnityEngine;
using System.Collections;

public class MoveTo : MonoBehaviour
{

    public Transform goal;
    UnityEngine.AI.NavMeshAgent agent;
    static bool abilityActive {get;set; }


    void Start()
    {
        abilityActive = false;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Update()
    {
        if (abilityActive == false)
        {
            agent.destination = goal.position;
            agent.gameObject.transform.LookAt(goal);
            agent.gameObject.transform.Rotate(new Vector3(0, 180, 0), Space.Self);
        }
    }

    //TODO use enumeration with which attack to use to decide what to do? which animation to play and what will happen?
    public static void startAbilityUsed()
    {
        Debug.Log("startAbilityUsed");
        abilityActive = true;
    }

    public static void endAbilityUsed()
    {
        Debug.Log("endAbilityUsed");
        abilityActive = false;
    }

    //move the wolf towards the selected game object and start attacking animation
    public void moveToAttack(GameObject target)
    {
        Debug.Log("moveToAttack in MoveTo script called");
        startAbilityUsed();
        //do everything
        endAbilityUsed();
    }

    //move the wolf towards the selected digging spot and start digging animation
    public void moveToDig(Vector3 point)
    {
        Debug.Log("moveToDig in MoveTo script called");
        startAbilityUsed();
        //do everything
        endAbilityUsed();
    }

    //move the wolf towards the selected tracking spot and start tracking animation
    public void moveToTrack(Vector3 point)
    {
        Debug.Log("moveToTrack in MoveTo script called");
        startAbilityUsed();
        //do everything
        endAbilityUsed();
    }

    //move the wolf towards the selected howl spot and start howling animation
    public void moveToHowl(Vector3 point)
    {
        Debug.Log("moveToHowl in MoveTo script called");
        startAbilityUsed();
        //do everything
        endAbilityUsed();
    }

    
}

