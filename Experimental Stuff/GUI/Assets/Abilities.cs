﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abilities : MonoBehaviour
{
    public Transform gunObj;
    Ability currentAbility;
    MoveTo moveTo;

    // Use this for initialization
    void Start()
    {
        currentAbility = Ability.Off;
        moveTo = GetComponent<MoveTo>();
    }

    // Update is called once per frame
    void Update()
    {
        //exit current action
        if (Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("escape current action");
            //also call exit in moveto
            currentAbility = Ability.Off;
        }

        if (currentAbility != Ability.Off)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("mouse click");
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    Debug.Log("hit");
                    Vector3 incomingVec = hit.point - gunObj.position;
                    Vector3 reflectVec = Vector3.Reflect(incomingVec, hit.normal);
                    Debug.DrawLine(gunObj.position, hit.point, Color.red);
                    Debug.DrawRay(hit.point, reflectVec, Color.green);

                    if (currentAbility == Ability.Attack)
                    {
                        moveTo.moveToAttack(hit.collider.gameObject);
                    }
                    else if (currentAbility == Ability.Dig)
                    {
                        moveTo.moveToDig(hit.point);
                    }
                    else if (currentAbility == Ability.Track)
                    {
                        moveTo.moveToTrack(hit.point);
                    }
                    else if (currentAbility == Ability.Howl)
                    {
                        moveTo.moveToHowl(hit.point);
                    }
                    else
                    {
                        Debug.LogError("current ability wasnt off but neither attack, dig, track or howl");
                    }
                }
                currentAbility = Ability.Off;
            }
        }
    }

    public void attack()
    {
        Debug.Log("attack button pressed on ui");
        currentAbility = Ability.Attack;
    }

    public void track()
    {
        Debug.Log("track button pressed on ui");
        currentAbility = Ability.Track; 
    }

    public void howl()
    {
        Debug.Log("howl button pressed on ui");
        currentAbility = Ability.Howl;
    }

    public void dig()
    {
        Debug.Log("dig button pressed on ui");
        currentAbility = Ability.Dig;
    }    

}
