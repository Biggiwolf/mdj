﻿
// MoveTo.cs
using UnityEngine;
using System.Collections;

public class MoveTo : MonoBehaviour
{

    public Transform goal;
	UnityEngine.AI.NavMeshAgent agent;


    void Start()
    {
       agent = GetComponent<UnityEngine.AI.NavMeshAgent>();   

    }

	void Update(){
		agent.destination = goal.position;
		agent.gameObject.transform.LookAt(goal);
		agent.gameObject.transform.Rotate(new Vector3(0, 180, 0), Space.Self);
	}
}

