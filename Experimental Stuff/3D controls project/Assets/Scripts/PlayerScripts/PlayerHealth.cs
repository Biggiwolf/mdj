﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public int startingHealth = 100;
    public int test = 10000;
    public int currentHealth;
    public Slider healthSlider;
    public bool drain = true;


	// Use this for initialization
	void Start () {
        currentHealth = startingHealth;
		
	}
	
	// Update is called once per frame
	void Update () {
        if (drain)
        {
            test--;
            currentHealth--;
            healthSlider.value = currentHealth;
        }
		
	}

    public void resetHealth()
    {
        test = 10000;
        currentHealth = 100;
        healthSlider.value = currentHealth;
    }

    public void activeDraining(bool active)
    {
        drain = active;
    }

    
}
